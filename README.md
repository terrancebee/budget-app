This app monitors the budget that I setup through the YNAB Budget app.

In order for this app to work locally, you need to set the `REACT_APP_API_KEY` in the `.env.local` file to a YNAB API person access token.

```
REACT_APP_API_KEY=[your API key here]
```