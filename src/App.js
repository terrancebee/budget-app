import React, { Component } from 'react';
import LoginContainer from "components/Login/LoginContainer";
import HomeContainer from "components/Home/HomeContainer";
import Brand from "components/Login/Brand";
import 'App.css';


class App extends Component {
  state = {
    email: "admin@example.com",
    password: "password",
    auth: false,
  }

  handleLogin = (e) => {
    console.log("in the login handler");

    const { email, password } = this.state
    const emailInput = e.target.querySelector("#user").value
    const passwordInput = e.target.querySelector("#pw").value

    if (email === emailInput && password === passwordInput) {
      this.setState({ auth: true })
    }
  }

  onBudgetLoaded = () => {
    const moneybag = document.querySelector(".ic-moneybag")
    moneybag.style.display = "none"
  }

  render() {
    const { auth } = this.state
    return (
        <div className="app-container container-fluid d-flex flex-column align-items-center">
          <Brand auth={auth} />
          { !auth
            ? <LoginContainer handleLogin={this.handleLogin} />
            : <HomeContainer onBudgetLoaded={this.onBudgetLoaded} />
          }
        </div>
    );
  }
}

export default App;