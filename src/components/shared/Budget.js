import React, { Component } from "react";

const YNAB_PATH_BASE = "https://api.youneedabudget.com/v1";
const YNAB_BUDGET = `${YNAB_PATH_BASE}/budgets`;
const ACCESS_KEY = process.env.REACT_APP_API_KEY;

class Budget extends Component {
  state = {
    budget: null,
    budgetID: null
  };

  componentDidMount() {
    if (!this._asyncRequest) {
      this._asyncRequest = this.fetchBudgetID();
    }
  }

  fetchBudgetID = async () => {
    try {
      const response = await fetch(`${YNAB_BUDGET}?access_token=${ACCESS_KEY}`);
      const result = await response.json();
      this.setState(state => ({
        budgetID: result.data.budgets[0].id
      }));
      return this.fetchBudget();
    }
    catch (e) {
      return e;
    }
  };

  fetchBudget = async () => {
    try {
      const response = await fetch(`${YNAB_BUDGET}/${this.state.budgetID}?access_token=${ACCESS_KEY}`);
      const result = await response.json();
      return this.setState(state => ({
        budget: result.data.budget
      }));
    }
    catch (e) {
      return e;
    }
  };

  render() {
    const { budget } = this.state;
    return (
      <React.Fragment>{budget && this.props.children(budget)}</React.Fragment>
    );
  }
}

export default Budget;
