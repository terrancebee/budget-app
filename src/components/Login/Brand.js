import React from "react";
import moneybag from "assets/images/money_bag.svg";

const Brand = ({ auth }) => {
  let classNames = `ic-moneybag mt-auto ${auth ? "is-animating mb-auto" : ""}`.trim();
  return (
    <object className={classNames} data={moneybag}>
      does not support object tags for SVGs
    </object>
  );
};

export default Brand;
