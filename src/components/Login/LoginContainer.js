import React, { Component } from "react";
import LoginView from "components/Login/LoginView";

class LoginContainer extends Component {

  render() {
    return (
      <React.Fragment>
        <LoginView {...this.props} />
        <a
          className="mt-auto"
          href="https://dryicons.com/icon/moneybag-icon-6445"
        >
          {" "}
          Icon by Dryicons{" "}
        </a>
      </React.Fragment>
    );
  }
}

export default LoginContainer;
