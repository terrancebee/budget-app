import React from "react";

const LoginForm = props => {
  const { handleLogin } = props;
  return (
    <form
      id="login-form"
      className="login-form text-right"
      onSubmit={handleLogin}
    >
      <div className="form-group">
        <input
          id="user"
          className="form-control"
          type="email"
          placeholder="email"
        />
      </div>
      <div className="form-group">
        <input
          id="pw"
          className="form-control"
          type="password"
          placeholder="password"
        />
      </div>
      <button className="btn btn-danger mt-3" type="submit">
        Log In
      </button>
    </form>
  );
};

export default LoginForm;
