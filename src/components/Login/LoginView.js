import React from "react";
import LoginForm from "components/Login/LoginForm";

const LoginView = props => {
  return <LoginForm {...props} />;
};

export default LoginView;
