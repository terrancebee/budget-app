import React from "react";

const MainView = props => {
  const { classes, displayedContent, defaultDisplay } = props;
  return (
    <div className={classes}>
      <div className="budget">
        <div className="budget-overview" />
        <div className="budget-details">
          {displayedContent || defaultDisplay()}
        </div>
      </div>
    </div>
  );
};

export default MainView;
