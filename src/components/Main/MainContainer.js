import React, { Component } from "react";
import MainView from "components/Main/MainView";

class MainContainer extends Component {
  render() {
    const classes = "col main-content";

    return <MainView classes={classes} {...this.props} />;
  }
}

export default MainContainer;
