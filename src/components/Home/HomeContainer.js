import React, { Component } from "react";
import HomeView from "components/Home/HomeView";
import BudgetGroup from "components/Table/BudgetGroup";
import BudgetRow from "components/Table/BudgetRow";
import TransactionHeader from "components/Table/TransactionHeader";
import TransactionRow from "components/Table/TransactionRow";
import Budget from "../shared/Budget";

class HomeContainer extends Component {
  state = {
    displayedContent: null
  };

  buildBudgetGroups = budget => {
    const budgetGroups = budget.category_groups.slice(2);
    return budgetGroups.map((categoryGroup, i) => (
      <BudgetGroup key={i} className="budget-group" name={categoryGroup.name}>
        {budget.categories
          .filter(c => c.category_group_id === categoryGroup.id)
          .slice(1)
          .map((category, i) => (
            <BudgetRow
              key={i}
              className="budget-row"
              budget={budget}
              {...category}
            />
          ))}
      </BudgetGroup>
    ));
  };

  buildTransactionsList = (budget, id) => {
    let list = budget.transactions;
    
    if (id) {
      list = budget.transactions.filter(t => t.account_id === id);
    }

    return (
      <TransactionHeader>
        {list.map((t, i) => (
          <TransactionRow
            key={i}
            categoryData={budget.categories}
            className="transaction-row"
            currencyFormat={budget.currency_format}
            payeeData={budget.payees}
            transaction={t}
          />
        ))}
      </TransactionHeader>
    );
  };

  updateView = arrayOfComponents => {
    this.setState(state => ({
      displayedContent: arrayOfComponents
    }));
  };

  showTransactions = (budget, id) => {
    this.updateView(this.buildTransactionsList(budget, id));
  };

  showBudget = budget => {
    this.updateView(this.buildBudgetGroups(budget));
  };

  render() {
    const { displayedContent } = this.state;
    let classes = "app row is-ready";

    return (
      <Budget>
        {budget => {
          return (
            <HomeView
              onLoad={this.props.onBudgetLoaded()}
              accounts={budget.accounts}
              budget={budget}
              classes={classes}
              defaultDisplay={() => this.buildBudgetGroups(budget)}
              displayedContent={displayedContent}
              showTransactions={this.showTransactions}
              showBudget={this.showBudget}
            />
          );
        }}
      </Budget>
    );
  }
}

export default HomeContainer;
