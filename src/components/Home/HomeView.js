import React from "react";
import SidebarContainer from "components/Sidebar/SidebarContainer";
import MainContainer from "components/Main/MainContainer";

const HomeView = props => {
  const {
    classes,
    displayedContent,
    defaultDisplay,
    accounts,
    budget,
    showTransactions,
    showBudget
  } = props;

  return (
    <div className={classes}>
      <SidebarContainer
        accounts={accounts}
        budget={budget}
        showTransactions={showTransactions}
        showBudget={showBudget}
      />
      <MainContainer
        displayedContent={displayedContent}
        defaultDisplay={defaultDisplay}
      />
    </div>
  );
};

export default HomeView;
