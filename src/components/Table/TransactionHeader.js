import React from 'react';

const TransactionHeader = props => (
  <div className="transactions">
    <div className="transactions-header transaction-row">
      <span>DATE</span>
      <span>PAYEE</span>
      <span>CATEGORY</span>
      <span>MEMO</span>
      <span>OUTFLOW</span>
      <span>INFLOW</span>
    </div>
    { props.children }
  </div>
);

export default TransactionHeader;