import React from "react";
// import Button from "components/Buttons/Button";

const BudgetGroup = props => {
  const { className, children, name } = props;

  return (
    <div className={className}>
      <div className="budget-row budget-group-name">
        <span>
          {name}
          {/* {" "}
          <Button
            className="btn"
            type="button"
            data-toggle="collapse"
            data-target={`#${name}`}
            aria-expanded="false"
            aria-controls="name"
          >
            toggle
          </Button> */}
        </span>
        <span>budgeted</span>
        <span>activity</span>
        <span>available</span>
      </div>
      <div id={name} className="collapse show">
        {children}
      </div>
    </div>
  );
};

export default BudgetGroup;
