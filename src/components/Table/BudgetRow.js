import React from "react";

const BudgetRow = ({
  className,
  name,
  budgeted,
  activity,
  balance,
  budget
}) => {
  const currency = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: budget.currency_format.iso_code,
    minimumFractionDigits: budget.currency_format.decimal_digits
  });
  return (
    <div className={className}>
      <span>{name}</span>
      <span>{currency.format(budgeted / 1000)}</span>
      <span>{currency.format(activity / 1000)}</span>
      <span>{currency.format(balance / 1000)}</span>
    </div>
  );
};

export default BudgetRow;
