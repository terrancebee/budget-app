import React from "react";

const TransactionRow = props => {
  const {
    currencyFormat,
    className,
    transaction,
    categoryData,
    payeeData
  } = props;

  const currency = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: currencyFormat.iso_code,
    minimumFractionDigits: currencyFormat.decimal_digits
  });

  const payee = payeeData.find(p => p.id === transaction.payee_id);

  let category;
  let cta;

  if (transaction.category_id === null) {
    if (transaction.transfer_account_id === null)
      cta = "This needs a category";
    else
      cta = "Doesn't need a category";
  } else {
    category = categoryData.find(c => c.id === transaction.category_id);
  }

  const amount = transaction.amount;

  return (
    <div className={className}>
      <span>{transaction.date}</span>
      <span>{payee ? payee.name : ''}</span>
      <span>{(category && category.name) || cta}</span>
      <span>{transaction.memo}</span>
      <span>{amount < 0 ? currency.format(amount / 1000) : null}</span>
      <span>{amount > 0 ? currency.format(amount / 1000) : null}</span>
    </div>
  );
};

export default TransactionRow;
