import React from "react";
import Button from "components/Buttons/Button";


const SidebarView = props => {
  const { accounts, budget, showBudget, showTransactions } = props;

  return (
      <div className="col sidebar">
        <div className="list-group list-group-flush">
          <Button
            className="list-group-item list-group-item-action h5"
            onClick={() => showBudget(budget)}
          >
            Budget
          </Button>
          <Button className="list-group-item list-group-item-action h5 disabled">
            All Accounts
          </Button>
          {accounts.map((a, i) => (
            <Button
              key={i}
              onClick={() => showTransactions(budget, a.id)}
              className="list-group-item list-group-item-action"
            >
              {a.name}
            </Button>
          ))}
        </div>
      </div>
  );
};

export default SidebarView;
