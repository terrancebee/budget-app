import React, { Component } from "react";
import SidebarView from "components/Sidebar/SidebarView";

class SidebarContainer extends Component {
  render() {
    const { accounts, budget, showTransactions, showBudget } = this.props;
    const displayedAccounts = accounts.filter(
      account => !account.closed && !account.deleted && account.on_budget
    );

    return (
      <SidebarView
        accounts={displayedAccounts}
        budget={budget}
        showTransactions={showTransactions}
        showBudget={showBudget}
      />
    )
  }
}

export default SidebarContainer;
